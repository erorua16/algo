const { forEach } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function ([...p]) {
    return p.filter((mp) => mp.gender == "Male");
  },

  allFemale: function ([...p]) {
    return p.filter((fp) => fp.gender == "Female");
  },

  nbOfMale: function ([...p]) {
    return this.allMale(p).length;
  },

  nbOfFemale: function ([...p]) {
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function ([...p]) {
    return p.filter((lpm) => lpm.looking_for == "M").length;
  },

  nbOfFemaleInterest: function ([...p]) {
    return p.filter((lpf) => lpf.looking_for == "F").length;
  },

  match: function ([...p]) {
    return "not implemented".red;
  },

  nbOfMoreThan2000$: function ([...p]) {
    return p.filter(function (money) {
      return parseInt(money.income.substring(1)) > 2000;
    }).length;
  },

  //   Nombre de personnes qui aiment les Drama : create function
  nbOfDramaLovers: function ([...p]) {
    return p.filter((drama) => drama.pref_movie.includes("Drama")).length;
  },

  //   Nombre de femmes qui aiment la science-fiction : create function
  nbOffemalescifiLovers: function ([...p]) {
    return this.allFemale(p).filter((scififemale) =>
      scififemale.pref_movie.includes("Sci-Fi")
    ).length;
  },

  // Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ : create function
  nbOfMoreThan1482$: function ([...p]) {
    return p.filter(function (money) {
      return parseFloat(money.income.substring(1)) > 1482.0;
    });
  },
  nbOf1482$documentaries: function ([...p]) {
    let i = this.nbOfMoreThan1482$(p);
    return i.filter((documentarymoney) =>
      documentarymoney.pref_movie.includes("Documentary")
    ).length;
  },
  // Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ : create function
  nbOfMoreThan4000$: function ([...p]) {
    const people = p.filter(function (money) {
      const argent = parseFloat(money.income.substring(1));
      return argent > 4000;
    });
    return people.map(
      (e) =>
        `First name : ${e.first_name} id : ${e.id} Last name: ${e.last_name}`
    );
  },
  // Homme le plus riche (nom et id) : create function
  richestDude: function ([...p]) {
    const richestMale = this.allMale([...p]).sort(
      (a, b) =>
        parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1))
    )[0];
    return `id : ${richestMale.id} first name : ${richestMale.first_name} last name : ${richestMale.last_name} income : ${richestMale.income}`;
  },

  // Salaire moyen : create function
  averageSalary: function (p) {
    total = 0;
    for (let i = 0; i < p.length; i++) {
      total += parseFloat(p[i].income.substring(1));
    }
    return (total / p.length).toFixed(2);
  },

  // Salaire médian : create function
  medianSalary: function ([...p]) {
    const sortedMoney = p.sort(
      (a, b) =>
        parseFloat(b.income.substring(1)) - parseFloat(a.income.substring(1))
    );
    let median = p.length / 2;
    let pair = parseFloat(sortedMoney[median].income.substring(1));
    let notPair =
      (parseFloat(sortedMoney[median].income.substring(1)) +
        parseFloat(sortedMoney[median - 1].income.substring(1))) /
      2;
    if (sortedMoney % 2 !== 0) {
      return pair.toFixed(2);
    } else {
      return notPair.toFixed(2);
    }
  },

  // Nombre de personnes qui habitent dans l'hémisphère nord : create function
  liveNorth: function ([...p]) {
    const inNorth = p.filter((north) => north.latitude >= 0);
    return inNorth.length;
  },

  // Salaire moyen des personnes qui habitent dans l'hémisphère sud : create function
  liveSouthMean: function ([...p]) {
    const inSouth = p.filter((South) => South.latitude < 0);
    total = 0;
    for (let i = 0; i < inSouth.length; i++) {
      total += parseFloat(inSouth[i].income.substring(1));
    }
    return (total / inSouth.length).toFixed(2);
  },

  // Personne qui habite le plus près de Bérénice Cawt (nom et id) : create function
  closeToBereniceCawt: function ([...p]) {
    let cawt = [];
    let alldistances = [];
    let allpeople = [];

    // getDistance: function (p, firstname_parm, lastname_parm) {
    //   let cawtArray = [];
    //   let allArray = [];
    //   let allDistance = [];

    //   // let lookForCawt = this.findSomeOne(p, name, lastname)

    //   p.map(function (e) {
    //     if (e.first_name == firstname_parm && e.last_name == lastname_parm) {
    //       cawtArray.push({
    //         latitude: e.latitude,
    //         longitude: e.longitude,
    //       });
    //     } else {
    //       allArray.push({
    //         latitude: e.latitude,
    //         longitude: e.longitude,
    //         lastname: e.last_name,
    //         name: e.first_name,
    //         id: e.id,
    //       });
    //     }
    //   });

    //   for (let i = 0; i < allArray.length; i++) {
    //     function calcul(lat1, lon1, lat2, lon2) {
    //       var R = 6371;
    //       var dLat = convert(lat2 - lat1);
    //       var dLon = convert(lon2 - lon1);
    //       var lat1 = convert(lat1);
    //       var lat2 = convert(lat2);

    //       var a =
    //         Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    //         Math.sin(dLon / 2) *
    //           Math.sin(dLon / 2) *
    //           Math.cos(lat1) *
    //           Math.cos(lat2);
    //       var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //       var d = R * c;
    //       return d;
    //     }
    //     function convert(Value) {
    //       return (Value * Math.PI) / 180;
    //     }
    //     let distanceConv = calcul(
    //       cawtArray[0].latitude,
    //       cawtArray[0].longitude,
    //       allArray[i].latitude,
    //       allArray[i].longitude
    //     );

    //     allDistance.push({
    //       distance: convert(distanceConv),
    //       lastname: allArray[i].lastname,
    //       first_name: allArray[i].name,
    //       id_inv: allArray[i].id,
    //     });
    //   }

    //   const orderDistance = allDistance.sort(function (a, b) {
    //     return a.distance - b.distance;
    //   });

    //   return orderDistance.map(
    //     e =>
    //       `La person plus proche a ${firstname_parm} ${lastname_parm} est ${
    //         e.first_name
    //       } ${e.lastname} aver l'id ${e.id_inv} qui est a une distance de ${
    //         Math.round(e.distance * 100 + Number.EPSILON) / 100
    //       } `
    //   )[0].blue;
    // },
  },

  // Personne qui habite le plus près de Ruì Brach (nom et id) : create function

  // les 10 personnes qui habite les plus près de Josée Boshard (nom et id) : create function

  // Les noms et ids des 23 personnes qui travaillent chez google : create function
  googleWorkers: function ([...p]) {
    const googleWorkers = p.filter((google) => google.email.includes("google"));
    return googleWorkers.map(
      (e) =>
        `First name : ${e.first_name} Last name: ${e.last_name} id : ${e.id}`
    );
  },

  // Personne la plus agée : create function
  oldestPerson: function ([...p]) {
    const sortedAge = p.sort(
      (a, b) => Date.parse(a.date_of_birth) - Date.parse(b.date_of_birth)
    )[0];
    // console.log(sortedAge)
    return `id : ${sortedAge.id} first name : ${sortedAge.first_name} last name : ${sortedAge.last_name} birthdate: ${sortedAge.date_of_birth}`;
  },

  // Personne la plus jeune : create function
  youngestPerson: function ([...p]) {
    const sortedAge = p.sort(
      (a, b) => Date.parse(b.date_of_birth) - Date.parse(a.date_of_birth)
    )[0];
    return `id : ${sortedAge.id} first name : ${sortedAge.first_name} last name : ${sortedAge.last_name} birthdate: ${sortedAge.date_of_birth}`;
  },

  // Moyenne des différences d'age : create function
  ageMean: function ([...p]) {
    let ages = [];
    let currentYear = new Date().getFullYear();
    const yearOfBirth = p.map((e) => e.date_of_birth.substring(0, 4));
    for (let i = 0; i < yearOfBirth.length; i++) {
      ages.push(currentYear - parseInt(yearOfBirth[i]));
    }
    total = 0;
    for (let i = 0; i < ages.length; i++) {
      total += ages[i];
    }
    return (total / ages.length).toFixed(2);
  },

  // Genre de film le plus populaire : create function
  mostPopularFilm: function ([...p]) {
    const filmnewp = p.map((e) => e.pref_movie);
    let idvFilmNewp = [];

    for (let i = 0; i < filmnewp.length; i++) {
      let split = filmnewp[i].split("|");
      for (let i = 0; i < split.length; i++) {
        idvFilmNewp.push(split[i]);
      }
    }

    let nbLikedFilms = [];
    idvFilmNewp.forEach((e) => {
      nbLikedFilms[e] = (nbLikedFilms[e] | 0) + 1;
    });

    const sortable = Object.entries(nbLikedFilms).sort(([, a], [, b]) => b - a);

    return sortable[0];
  },

  // Genres de film par ordre de popularité : create function
  popularFilmRanking: function ([...p]) {
    const filmnewp = p.map((e) => e.pref_movie);
    let idvFilmNewp = [];

    for (let i = 0; i < filmnewp.length; i++) {
      let split = filmnewp[i].split("|");
      for (let i = 0; i < split.length; i++) {
        idvFilmNewp.push(split[i]);
      }
    }

    let nbLikedFilms = [];
    idvFilmNewp.forEach((e) => {
      nbLikedFilms[e] = (nbLikedFilms[e] | 0) + 1;
    });

    const sortable = Object.entries(nbLikedFilms).sort(([, a], [, b]) => b - a);

    return sortable;
  },

  // Liste des genres de film et nombre de personnes qui les préfèrent : create function
  filmGenreAndNbPreference([...p]) {
    const filmnewp = p.map((e) => e.pref_movie);
    let idvFilmNewp = [];
    // let numberOfFilmpeopleLike = [];

    for (let i = 0; i < filmnewp.length; i++) {
      let split = filmnewp[i].split("|");
      for (let i = 0; i < split.length; i++) {
        idvFilmNewp.push(split[i]);
      }
    }

    // console.log(idvFilmNewp);

    // const filteredgenre = idvFilmNewp.filter(
    //   (a, b) => idvFilmNewp.indexOf(a) == b
    // );

    let nbLikedFilms = [];
    idvFilmNewp.forEach((e) => {
      nbLikedFilms[e] = (nbLikedFilms[e] | 0) + 1;
    });
    return nbLikedFilms;

    // const sortable = Object.entries(nbLikedFilms)
    //   .sort(([, a], [, b]) => b - a)

    // console.log(sortable);
  },

  // Age moyen des hommes qui aiment les films noirs : create function
  meanOfManLikePolar([...p]) {
    const likePolar = this.allMale(p).filter((e) =>
      e.pref_movie.includes("Film-Noir")
    );
    
    let polarMaleAges = [];
    let currentYear = new Date().getFullYear();
    const yearOfBirth = likePolar.map((e) => e.date_of_birth.substring(0, 4));
    for (let i = 0; i < yearOfBirth.length; i++) {
      polarMaleAges.push(currentYear - parseInt(yearOfBirth[i]));
    }
    total = 0;
    for (let i = 0; i < polarMaleAges.length; i++) {
      total += polarMaleAges[i];
    }
    return (total / polarMaleAges.length).toFixed(2);
  },


  // Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire de Paris et gagnent moins que la moyenne des hommes : create function



  // Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une préférence de film en commun (afficher les deux et la distance entre les deux): create function



  // Liste des couples femmes / hommes qui ont les même préférences de films : create function
};
